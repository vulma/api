package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/justinas/alice"

	flags "github.com/jessevdk/go-flags"

	"github.com/rs/rest-layer/resource"
	"github.com/rs/rest-layer/rest"
	"github.com/rs/rest-layer/schema"

	"github.com/apuigsech/rest-layer-sql"
	_ "github.com/lib/pq"

	"github.com/apuigsech/rest-layer-openapi"
	"github.com/getkin/kin-openapi/openapi3"

	"gitlab.com/vulma/api/auth"
)

const (
	ApiName    = "guardwave"
	ApiVersion = "1.0"
)

var options struct {
	Host        string `long:"host" description:"the IP to listen on" default:"localhost" env:"HOST"`
	Port        int    `long:"port" description:"the port to listen on for insecure connections, defaults to a random value" env:"PORT"`
	ShowOpenapi bool   `long:"show-openapi" description:"shows the openapi definition for the API"`
}

var (
	namespace = schema.Schema{
		Fields: schema.Fields{
			"id":      schema.IDField,
			"created": schema.CreatedField,
			"updated": schema.UpdatedField,
			"name": {
				Required:   true,
				Filterable: true,
				Validator: &schema.String{
					MinLen: 3,
					MaxLen: 512,
				},
			},
		},
	}

	member = schema.Schema{
		Fields: schema.Fields{
			"id":      schema.IDField,
			"created": schema.CreatedField,
			"updated": schema.UpdatedField,
			"namespace": {
				Required:   true,
				Filterable: true,
				Validator: &schema.Reference{
					Path: "namespaces",
				},
			},
			"username": {
				Required:   true,
				Filterable: true,
				Validator: &schema.String{
					MinLen: 1,
					MaxLen: 512,
				},
			},
			"roles": {
				Required: true,
				Validator: &schema.Array{
					Values: schema.Field{
						Validator: &schema.String{
							MaxLen: 512,
						},
					},
				},
			},
		},
	}

	setting = schema.Schema{
		Fields: schema.Fields{
			"id":      schema.IDField,
			"created": schema.CreatedField,
			"updated": schema.UpdatedField,
			"namespace": {
				Required:   true,
				Filterable: true,
				Validator: &schema.Reference{
					Path: "namespaces",
				},
			},
			"key": {
				Required: true,
				Validator: &schema.String{
					MinLen: 1,
					MaxLen: 512,
				},
			},
			"value": {
				Required: true,
				Validator: &schema.String{
					MaxLen: 512,
				},
			},
		},
	}

	asset = schema.Schema{
		Fields: schema.Fields{
			"id":      schema.IDField,
			"created": schema.CreatedField,
			"updated": schema.UpdatedField,
			"namespace": {
				Required:   true,
				Filterable: true,
				Validator: &schema.Reference{
					Path: "namespaces",
				},
			},
			"origin": {
				Required: false,
				Validator: &schema.String{
					MaxLen: 128,
				},
				Filterable: true,
			},
			"type": {
				Required: true,
				Validator: &schema.String{
					MaxLen: 64,
				},
				Filterable: true,
			},
			"value": {
				Required: true,
				Validator: &schema.String{
					MaxLen: 512,
				},
				Filterable: true,
			},
			"env_cvss": {
				Required: true,
				Default:  "CVSS:3.0/CR:X/IR:X/AR:X/MAV:X/MAC:X/MPR:X/MUI:X/MS:X/MC:X/MI:X/MA:X",
				Validator: &schema.String{
					MaxLen: 128,
					Regexp: "^CVSS:3.0/CR:([XHML])/IR:([XHML])/AR:([XHML])/MAV:([XNALP])/MAC:([XLH])/MPR:([XNLH])/MUI:([XNR])/MS:([XUC])/MC:([XHLN])/MI:([XHLN])/MA:([XHLN])$",
				},
				Filterable: true,
			},
		},
	}

	asset_setting = schema.Schema{
		Fields: schema.Fields{
			"id":      schema.IDField,
			"created": schema.CreatedField,
			"updated": schema.UpdatedField,
			"namespace": {
				Required:   true,
				Filterable: true,
				Validator: &schema.Reference{
					Path: "namespaces",
				},
			},
			"key": {
				Required: true,
				Validator: &schema.String{
					MinLen: 1,
					MaxLen: 512,
				},
			},
			"value": {
				Required: true,
				Validator: &schema.String{
					MaxLen: 512,
				},
			},
		},
	}
)

type StorerMap map[string]resource.Storer

func prepareIndex(storers StorerMap) (resource.Index,resource.Index) {
	admin_index := resource.NewIndex()
	user_index := resource.NewIndex()


	members := admin_index.Bind("members", member, storers["members"], resource.Conf{
		AllowedModes: resource.ReadOnly,
	})




	namespaces := user_index.Bind("namespaces", namespace, storers["namespaces"], resource.Conf{
		AllowedModes: resource.ReadWrite,
	})

	namespaces.Use(auth.SetDefaultRolesHook{
		Members: members,
		Roles:   []string{"master"},
	})



	namespaces_members := namespaces.Bind("members", "namespace", member, storers["members"], resource.Conf{
		AllowedModes: resource.ReadWrite,
	})

	namespaces_members.Use(auth.AuthorizeHook{
		Members:       members,
		MasterRole:    "master",
		ReadOnlyRole:  "members-readonly",
		ReadWriteRole: "members-readwrite",
	})




	namespaces_settings := namespaces.Bind("settings", "namespace", setting, storers["settings"], resource.Conf{
		AllowedModes: resource.ReadWrite,
	})

	namespaces_settings.Use(auth.AuthorizeHook{
		Members:       members,
		MasterRole:    "master",
		ReadOnlyRole:  "settings-readonly",
		ReadWriteRole: "settings-readwrite",
	})



	namespaces_assets := namespaces.Bind("assets", "namespace", asset, storers["assets"], resource.Conf{
		AllowedModes: resource.ReadWrite,
	})

	namespaces_assets.Use(auth.AuthorizeHook{
		Members:       members,
		MasterRole:    "master",
		ReadOnlyRole:  "assets-readonly",
		ReadWriteRole: "assets-readwrite",
	})



	namespaces_assets_settings := namespaces.Bind("settings", "assets", asset_setting,  storers["asset_settings"], resource.Conf{
		AllowedModes: resource.ReadWrite,
	})

	namespaces_assets_settings.Use(auth.AuthorizeHook{
		Members:       members,
		MasterRole:    "master",
		ReadOnlyRole:  "assets-readonly",
		ReadWriteRole: "assets-readwrite",
	})


	return user_index, admin_index
}

func prepareDoc(index resource.Index) *openapi3.Swagger {
	doc := openapi.NewOpenapiFromIndex(index, openapi3.Info{
		Title:   ApiName,
		Version: ApiVersion,
	})

	doc.Security = openapi3.SecurityRequirements{
		{
			"auth": []string{
				"foo:bar",
			},
		},
	}

	return doc
}

func main() {
	os.Setenv("HOST", os.Getenv("SERVICE_HOST"))
	os.Setenv("PORT", os.Getenv("SERVICE_PORT"))

	var parser = flags.NewParser(&options, flags.Default)

	if _, err := parser.Parse(); err != nil {
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	}

	var storers StorerMap
	var err error
	storers = make(map[string]resource.Storer)

	if options.ShowOpenapi {
		storers["namespaces"] = nil
		storers["members"] = nil
		storers["settings"] = nil
		storers["assets"] = nil
		storers["asset_settings"] = nil
	} else {
		dbDriver := os.Getenv("DB_DRIVER")
		dbSource := os.Getenv("DB_SOURCE")

		storers["namespaces"], err = sqlStorage.NewHandler(dbDriver, dbSource, "namespaces")
		if err != nil {
			log.Fatal(err)
		}

		storers["members"], err = sqlStorage.NewHandler(dbDriver, dbSource, "members")
		if err != nil {
			log.Fatal(err)
		}

		storers["settings"], err = sqlStorage.NewHandler(dbDriver, dbSource, "settings")
		if err != nil {
			log.Fatal(err)
		}

		storers["assets"], err = sqlStorage.NewHandler(dbDriver, dbSource, "assets")
		if err != nil {
			log.Fatal(err)
		}

		storers["asset_settings"], err = sqlStorage.NewHandler(dbDriver, dbSource, "asset_settings")
		if err != nil {
			log.Fatal(err)
		}
	}

	index,_ := prepareIndex(storers)

	if options.ShowOpenapi {
		doc := prepareDoc(index)
		b, _ := doc.MarshalJSON()
		fmt.Println(string(b))
		return
	}

	mdw := alice.New(auth.AuthenticateMiddleware)

	api, err := rest.NewHandler(index)
	if err != nil {
		log.Fatalf("Invalid API configuration: %s", err)
	}

	http.Handle("/", mdw.Then(api))

	serverString := fmt.Sprintf("%s:%d", options.Host, options.Port)

	log.Printf("Server running on %s", serverString)
	if err := http.ListenAndServe(serverString, nil); err != nil {
		log.Fatal(err)
	}
}
