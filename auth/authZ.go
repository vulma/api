package auth

import (
	"context"
	"net/http"
	"strings"

	"github.com/rs/rest-layer/resource"
	"github.com/rs/rest-layer/rest"
	"github.com/rs/rest-layer/schema/query"
)

var ErrForbidden = &rest.Error{http.StatusForbidden, "Forbidden", nil}

type AuthorizeHook struct {
	Members       *resource.Resource
	MasterRole    string
	ReadOnlyRole  string
	ReadWriteRole string
}

func (h AuthorizeHook) OnFind(ctx context.Context, q *query.Query) error {
	username := UsernameFromContext(ctx)
	if username == "" {
		return resource.ErrForbidden
	}

	var namespace string
	for _, exp := range q.Predicate {
		eq, ok := exp.(*query.Equal)
		if ok {
			if eq.Field == "namespace" {
				namespace = eq.Value.(string)
			}
		}
	}

	return h.checkAuthorization(ctx, username, namespace, []string{
		h.MasterRole,
		h.ReadOnlyRole,
		h.ReadWriteRole,
	})
}

func (h AuthorizeHook) OnGet(ctx context.Context, id interface{}) error {
	username := UsernameFromContext(ctx)
	if username == "" {
		return resource.ErrForbidden
	}

	namespace := id.(string)

	return h.checkAuthorization(ctx, username, namespace, []string{
		h.MasterRole,
		h.ReadOnlyRole,
		h.ReadWriteRole,
	})
}

func (h AuthorizeHook) OnInsert(ctx context.Context, items []*resource.Item) error {
	username := UsernameFromContext(ctx)
	if username == "" {
		return resource.ErrForbidden
	}

	for _, item := range items {
		namespace := item.Payload["namespace"].(string)

		err := h.checkAuthorization(ctx, username, namespace, []string{
			h.MasterRole,
			h.ReadWriteRole,
		})
		if err != nil {
			return err
		}

	}
	return nil
}

func (h AuthorizeHook) OnUpdate(ctx context.Context, item *resource.Item, original *resource.Item) error {
	username := UsernameFromContext(ctx)
	if username == "" {
		return resource.ErrForbidden
	}

	namespace := item.Payload["namespace"].(string)

	return h.checkAuthorization(ctx, username, namespace, []string{
		h.MasterRole,
		h.ReadWriteRole,
	})
}

func (h AuthorizeHook) OnDelete(ctx context.Context, item *resource.Item) error {
	username := UsernameFromContext(ctx)
	if username == "" {
		return resource.ErrForbidden
	}

	namespace := item.Payload["namespace"].(string)

	return h.checkAuthorization(ctx, username, namespace, []string{
		h.MasterRole,
		h.ReadWriteRole,
	})
}

func (h AuthorizeHook) OnClear(ctx context.Context, q *query.Query) error {
	username := UsernameFromContext(ctx)
	if username == "" {
		return resource.ErrForbidden
	}

	var namespace string
	for _, exp := range q.Predicate {
		eq, ok := exp.(*query.Equal)
		if ok {
			if eq.Field == "namespace" {
				namespace = eq.Value.(string)
			}
		}
	}

	return h.checkAuthorization(ctx, username, namespace, []string{
		h.MasterRole,
		h.ReadWriteRole,
	})
}

func (h AuthorizeHook) getRoles(ctx context.Context, username string, namespace string) (roles []string, err error) {
	items, err := h.Members.Find(ctx, &query.Query{
		Predicate: query.Predicate{
			&query.Equal{Field: "username", Value: username},
			&query.Equal{Field: "namespace", Value: namespace},
		},
		Window: &query.Window{Limit: 1},
	})

	if err != nil {
		return
	}
	if len(items.Items) != 1 {
		return
	}

	r := items.Items[0].Payload["roles"].(string)
	roles = strings.Split(r[1:len(r)-1], ",")

	return
}

func (h AuthorizeHook) checkAuthorization(ctx context.Context, username string, namespace string, authorizedRoles []string) error {
	roles, err := h.getRoles(ctx, username, namespace)
	if err != nil {
		return err
	}

	for _, r1 := range roles {
		for _, r2 := range authorizedRoles {
			if r1 == r2 {
				return nil
			}
		}
	}

	return resource.ErrForbidden
}

func getNamespaceFromQuery(q *query.Query) (string, error) {
	var namespace string
	for _, exp := range q.Predicate {
		eq, ok := exp.(*query.Equal)
		if ok {
			if eq.Field == "namespace" {
				namespace = eq.Value.(string)
				return namespace, nil
			}
		}
	}
	return "", nil // TODO
}
