package auth

import (
	"context"

	"github.com/rs/rest-layer/resource"
)

type SetDefaultRolesHook struct {
	Members *resource.Resource
	Roles   []string
}

func (h SetDefaultRolesHook) OnInserted(ctx context.Context, items []*resource.Item, err *error) {
	username := UsernameFromContext(ctx)

	var listRoles []interface{}
	for _, role := range h.Roles {
		listRoles = append(listRoles, string(role))
	}

	for _, item := range items {
		memberPayload := map[string]interface{}{
			"username":  username,
			"namespace": item.ID,
			"roles":     listRoles,
		}

		changes, base := h.Members.Validator().Prepare(ctx, memberPayload, nil, false)
		doc, errs := h.Members.Validator().Validate(changes, base)
		if len(errs) > 0 {
			continue
		}

		memberItem, err := resource.NewItem(doc)
		if err != nil {
			continue
		}

		if err = h.Members.Insert(ctx, []*resource.Item{memberItem}); err != nil {
			continue
		}
	}
}
