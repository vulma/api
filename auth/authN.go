package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/rest-layer/rest"
	"net/http"
	"strings"
)

const USER_HEADER = "X-Forwarded-User"
const GROUPS_HEADER = "X-Forwarded-Groups"

var ErrUnautorized = &rest.Error{http.StatusUnauthorized, "Unauthorized", nil}

type AuthInfo struct {
	Username string
	Groups   []string
}

func NewAuthInfo(h http.Header) (*AuthInfo, error) {
	authInfo := &AuthInfo{}

	authInfo.Username = h.Get(USER_HEADER)
	authInfo.Groups = strings.Split(h.Get(GROUPS_HEADER), ",")

	if authInfo.Username == "" {
		return nil, errors.New("Invalid Username")
	}

	return authInfo, nil
}

func AuthenticateMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := NewAuthInfo(r.Header)
		if err != nil {
			j, err := json.Marshal(ErrUnautorized)
			if err != nil {
				w.WriteHeader(500)
				msg := fmt.Sprintf("Can't build response: %q", err.Error())
				w.Write([]byte(fmt.Sprintf("{\"code\": 500, \"msg\": \"%s\"}", msg)))
				return
			}
			http.Error(w, string(j), http.StatusUnauthorized)
			return
		}

		ctx := ContextWithAuthInfo(r.Context(), authInfo)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func ContextWithAuthInfo(ctx context.Context, authUser *AuthInfo) context.Context {
	return context.WithValue(ctx, "AuthInfo", authUser)
}

func AuthInfoFromContext(ctx context.Context) (*AuthInfo, error) {
	authInfo, ok := ctx.Value("AuthInfo").(*AuthInfo)
	if !ok {
		return nil, errors.New("Invalid AuthInfo")
	}
	return authInfo, nil
}

func UsernameFromContext(ctx context.Context) string {
	authInfo, _ := AuthInfoFromContext(ctx)
	return authInfo.Username
}

func GroupsFromContext(ctx context.Context) []string {
	authInfo, _ := AuthInfoFromContext(ctx)
	return authInfo.Groups
}
