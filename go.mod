module gitlab.com/vulma/api

require (
	github.com/apuigsech/rest-layer-openapi v0.0.0-20190514180514-a9cf8e344d12
	github.com/apuigsech/rest-layer-sql v0.0.0-20190514120652-34db37f574e1
	github.com/davecgh/go-spew v1.1.1
	github.com/getkin/kin-openapi v0.1.1-0.20190404145759-2fa3ddc203b1
	github.com/jessevdk/go-flags v1.4.0
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/justinas/alice v0.0.0-20171023064455-03f45bd4b7da
	github.com/lib/pq v1.1.1
	github.com/rs/rest-layer v0.1.1-0.20190408091018-f3fd620c1030
	golang.org/x/crypto v0.0.0-20190411191339-88737f569e3a // indirect
)
